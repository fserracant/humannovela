# Human NonVerbal Language Dataset #

The goal of Human NonVerbal Language Dataset (HumanNoVeLa) is to gather data related to human body features that can hopefully be used to study nonverbal language characteristics.

### What can be found? ###


* Entrepreneurs dataset: contains data from 218 entrepreneurial pitching sessions in the form of 3D body pose estimation and nonverbal characteristics.
* Feel free to contact us if you want to add extra datasets! :)

### Contribution guidelines ###

* ToDo

### Who do I talk to? ###

* francesc.serracant@gmail.com
