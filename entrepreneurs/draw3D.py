import sys
import pickle
import glob
import os
import re
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
from matplotlib import animation
import cv2
import argparse

parser = argparse.ArgumentParser(description='Draw 3D animation of entrepreneurs skeletons')
parser.add_argument('id')
args = parser.parse_args()

f = open("dataset.pkl", 'rb')
data = pickle.load(f)
f.close()


'''
0 center hip
1 right hip
2 right knee
3 right foot
4 left hip
5 left knee
6 left hip
7 spine
8 neck
9 nose
10 eyes
11 left shoulder
12 left elbow
13 left hand
14 right shoulder
15 right elbow
16 right hand
'''
def draw3Dline(ax, a, b):
    xs = [a[0], b[0]] 
    ys = [a[1], b[1]] 
    zs = [a[2], b[2]] 

    ax.plot(xs, ys, zs)

def draw3Dpose(ax, pose):

    Xs, Ys, Zs = pose[:,0], pose[:,1], pose[:,2]
    ax.scatter(Xs, Ys, Zs, zorder=10)

    # right leg
    draw3Dline(ax, pose[0], pose[1])
    draw3Dline(ax, pose[1], pose[2])
    draw3Dline(ax, pose[2], pose[3])

    # left leg
    draw3Dline(ax, pose[0], pose[4])
    draw3Dline(ax, pose[4], pose[5])
    draw3Dline(ax, pose[5], pose[6])

    # spine
    draw3Dline(ax, pose[0], pose[7])
    draw3Dline(ax, pose[7], pose[8])

    # head
    draw3Dline(ax, pose[8], pose[9])
    draw3Dline(ax, pose[9], pose[10])

    # left arm
    draw3Dline(ax, pose[8], pose[11])
    draw3Dline(ax, pose[11], pose[12])
    draw3Dline(ax, pose[12], pose[13])

    # right arm
    draw3Dline(ax, pose[8], pose[14])
    draw3Dline(ax, pose[14], pose[15])
    draw3Dline(ax, pose[15], pose[16])

plt.rcParams['animation.ffmpeg_path'] = '/home/cesc/anaconda3/bin/ffmpeg'

id = int(args.id)
speaker = [s for s in data['sequences'] if s['info']['speakerId'] == id]

if len(speaker) == 0:
    print("Id ", id, "not found")
    sys.exit(1)

seq = speaker[0]['frames']

animationFile = str(id) + ".mpg"
print("Writing ", animationFile)

fig = plt.figure()

def update(num):
    plt.clf()
    radius = 1.7
    azim = 70
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(elev=15., azim=azim)
    ax.set_xlim3d([-radius/2, radius/2])
    ax.set_zlim3d([0, radius])
    ax.set_ylim3d([-radius/2, radius/2])
    ax.set_aspect('auto')
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_zticklabels([])

    draw3Dpose(ax, np.asarray(seq[num]))

FPS = speaker[0]['info']['FPS']

print(FPS)

N = len(seq)
ani = animation.FuncAnimation(fig, update, N, interval=10000/N, blit=False)
FFwriter = animation.FFMpegWriter(fps=int(FPS), extra_args=['-vcodec', 'libx264'])
ani.save(animationFile, writer = FFwriter)
plt.close()
plt.clf()
