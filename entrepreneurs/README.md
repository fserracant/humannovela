# Entrepreneurs dataset #

This dataset includes data taken from 218 entrepreneur pitching sessions. From those sessions we took 3D pose estimation of each entrepreneur and included ground truth data about personal nonverbal characteristics during their speaches.
Dataset is packed using python's pickle format and contains a dictionary with the following structure.

### Data structure ###
 
* info: a dictionary with general information about the dataset
* sequences: a list of sequenceds, being each sequence a pitching session
	* info: a dictionary with information about the sequence
		* speakerId
		* FPS: frames per second of the sequence
	* frames: order list of 3D poses (18 joints) for every frame
		* 0 center hip
		* 1 right hip
		* 2 right knee
		* 3 right foot
		* 4 left hip
		* 5 left knee
		* 6 left hip
		* 7 spine
		* 8 neck
		* 9 nose
		* 10 eyes
		* 11 left shoulder
		* 12 left elbow
		* 13 left hand
		* 14 right shoulder
		* 15 right elbow
		* 16 right hand
		* 17 0
	* attributes: ditctinary with personal nonverbal attributes
		* attractiveness
		* competence
		* dominance
		* openness
		* passion
		* trustworthiness
